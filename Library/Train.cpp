#include "stdafx.h"
#include "Train.h"
#include <iostream>

using namespace std;

void Wagon::check(int n, int m){
	if (n > m || n < 0 || m < 0)
		throw "Incorrect data!!!";
}

Train::Train(const Wagon & wagon){
	number = 0;
	if (wagon.number > wagon.max || wagon.number < 0 || wagon.max < 0)
		throw "Incorrect data!!!";
	if (number < maxN)
		train[number] = wagon;
	number++;
}

istream & operator >> (istream & c, Train & train){//���� ����������� ������ �� �������� ������
	int number, max;
	for (int i = 0; i < train.maxN; i++){
		c >> max;
		if (c.eof() || !c.good())
			return c;
		c >> number;
		if (c.eof() || !c.good())
			return c;
		if (max < 0 || number < 0 || max < number){
			c.setstate(ios_base::failbit);
			return c;
		}
		train.number++;
		train.train[i].max = max;
		train.train[i].number = number;
	}
	return c;
}

ostream & operator << (ostream & c, const Train & train){ //����� ���������� � ��������� ������� � �������� �����
	c << "Number of passengers in wagons: " << endl;
	for (int i = 0; i < train.number; i++)
		c << "The " << i+1 << "th wagon: " << train.train[i].number << endl;
	return c;
}

Train & Train::operator () (int n, int places){// "������ �����(��������� ����)" � ������ � ��������� �������
	if (n <= 0 || n > number || places < 0)
		throw "Incorrect data!!!";
	if (places > (train[n-1].max - train[n-1].number))
		throw "No free places!!!";
	train[n -1].number += places;
	return * this;
}

Wagon Train::get_wagon(int index)const{
	if (index > number || index <= 0)
		throw "Incorrect data!!!";
	return train[index - 1];
}

int Train::operator [] (int n)const{ //���������� ���������� ������� ���� � ������ � ��������� �������
	if (n <= 0 || n > number)
		throw "Incorrect data!!!";
	return train[n - 1].number;
}

Train & Train::operator += (const Train & train1){ //�������� ����� � ������
	if (train1.number < maxN - number)
		for (int i = 0; i < train1.number; i++)
			train[number + i] = train1.train[i];
	else
		throw "Wagon can't be added!!!";
	number += train1.number;
	return *this;
}

void Train::operator () (int n, const int index[], Train & new_train){ // �������� ������ (� ���������� �������������� ������ ����� � ���������� �����)
	Train t;
	new_train.number = 0;
	int i, j;
	if (n < 0)
		throw "Error!!! n < 0!!!";
	if(n > number)
		throw "Error n can't be more then number of wagons in train!!!";
	for (i = 0; i < n; i++)
		if (index[i] <= 0 || index[i] > number)
			throw "Incorrect index!!!";
	for (i = 0; i < n; i++){
		t.Wagon_to_train(train[index[i] - 1]);
		new_train += t;
		train[index[i] - 1].max = -1;
	}
	for (i = 0; i < number && train[i].max != -1; i++);
	for (i = i + 1; i < number; i++)
		if (train[i].max != -1){
			for (j = i - 1; j >= 0 && train[j].max == -1; j--);
			train[j + 1] = train[i];
			train[i].max = -1;
		}
	number -= n;
	new_train.number = n;
}

int Train::Passengers()const{ //���������� ��������� � ������
	int S = 0;
	for (int i = 0; i < number; i++)
		S += train[i].number;
	return S;
}

void Train::Wagon_to_train(const Wagon & wagon){
	number = 0;
	if (wagon.number > wagon.max || wagon.number < 0 || wagon.max < 0)
		throw "Incorrect data!!!";
	if (number < maxN)
		train[number] = wagon;
	else
		throw "Wagon can't be added!!!";
	number++;
}



istream & Train::Initialization(istream & c){ //���� ����������� ������ �� �������� ������
	int number, max;
	number = 0;
	for (int i = 0; i < maxN; i++){
		do{
			c >> max;
			if (c.eof()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				return c;
			}
			if (!c.good()){
				max = -1;
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
			}
			c >> number;
			if (c.eof()){
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				return c;
			}
			if (!c.good()){
				max = -1;
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
			}
			number++;
		} while (max < 0 || number < 0 || number > max);
		train[i].max = max;
		train[i].number = number;
	}
}

ostream & Train::Passengers_in_wagons(ostream & c){ //����� ���������� � ��������� ������� � �������� �����
	c << "Number of passengers in wagons: " << endl;
	for (int i = 0; i < number; i++)
		c << "The " << i + 1 << "th wagon: " << train[i].number << endl;
	return c;
}

Train & Train::Take_place(int n, int places){// "������ �����(��������� ����)" � ������ � ��������� �������
	if (n <= 0 || n > number || places < 0)
		throw "Incorrect data!!!";
	if (places >(train[n - 1].max - train[n - 1].number))
		throw "No free places!!!";
	train[n - 1].number += places;
	return *this;
}

int Train::Passengers_in_wagon(int number){ //���������� ���������� ������� ���� � ������ � ��������� �������
	if (number <= 0 || number > maxN)
		throw "Incorrect data!!!";
	return train[number - 1].number;
}

Train & Train::Add_wagon(const Train& train1){ //�������� ����� � ������
	if (train1.number < maxN - number)
		for (int i = 0; i < train1.number; i++)
			train[number + i] = train1.train[i];
	else
		throw "Wagon can't be added!!!";
	number += train1.number;
	return *this;
}

void Train::Replace_wagons(int n, const int index[], Train & new_train){ // �������� ������ (� ���������� �������������� ������ ����� � ���������� �����)
	Train t;
	new_train.number = 0;
	int i, j;
	if (n < 0 || n > number)
		throw "Incorrect data!!!";
	for (i = 0; i < n; i++)
		if (index[i] <= 0 || index[i] > number)
			throw "Incorrect data!!!";
	for (i = 0; i < n; i++){
		t.Wagon_to_train(train[index[i] - 1]);
		new_train += t;
		train[index[i] - 1].max = -1;
	}
	for (i = 0; i < number && train[i].max != -1; i++);
	for (i = i + 1; i < number; i++)
		if (train[i].max != -1){
			for (j = i - 1; j >= 0 && train[j].max == -1; j--);
			train[j + 1] = train[i];
			train[i].max = -1;
		}
	number -= n;
	new_train.number = n;
}