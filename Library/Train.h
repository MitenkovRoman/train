#pragma once
#include <iostream>
using namespace std;

struct Wagon{//��������� �������� ������
private:
	void check(int n, int m); //�������� max � number
public:
	int max; //������������ �����������
	int number; //���������� ������� ����
	Wagon(int m = 50, int n = 0){ check(n, m); number = n;  max = m; } //���������������� ����������� ��� ��������� ������
	~Wagon(){}; //����������
};

class Train{
private:
	static const int maxN = 10; //������������ ���������� �������
	int number; //���������� �������
	Wagon train[maxN];//������ ������� (�����)
public:
	Train() : number(0){}//������ ����������� ��� ������������� ����������� ������
	Train(const Wagon&);                                                                                                                                                                                              
	~Train(){}; //����������
	friend istream & operator >> (istream &, Train &); //���� ����������� ������ �� �������� ������
	friend ostream & operator << (ostream &, const Train &); //����� ���������� � ��������� ������� � �������� �����
	int get_maxN() const{ return maxN; }
	int get_number()const{ return number; }
	Wagon get_wagon(int index)const;
	Train & operator () (int number, int places);// "������ �����(��������� ����)" � ������ � ��������� �������
	int operator [] (int number)const; //���������� ���������� ������� ���� � ������ � ��������� �������
	Train & operator += (const Train&); //�������� ����� � ������
	void operator () (int number, const int index[], Train&); // �������� ������ (� ���������� �������������� ������ ����� � ���������� �����)
	void Train::Wagon_to_train(const Wagon & wagon);

	istream & Initialization(istream &); //���� ����������� ������ �� �������� ������
	ostream & Passengers_in_wagons(ostream &); //����� ���������� � ��������� ������� � �������� �����
	Train & Take_place (int number, int places);// "������ �����(��������� ����)" � ������ � ��������� �������
	int Passengers_in_wagon(int number); //���������� ���������� ������� ���� � ������ � ��������� �������
	Train & Add_wagon(const Train &); //�������� ����� � ������
	void Train::Replace_wagons(int n, const int index[], Train & new_train); // �������� ������ (� ���������� �������������� ������ ����� � ���������� �����)

	int Passengers() const; //���������� ��������� � ������
};
