#include "stdafx.h"
#include "..\Library\Train.h"
#include "gtest\gtest.h"
#include <sstream>

TEST(Constructors, InitConstructor){
	Train train1;
	EXPECT_EQ(0, train1.get_number());
	EXPECT_EQ(10, train1.get_maxN());
	Wagon wagon1;
	EXPECT_EQ(0, wagon1.number);
	EXPECT_EQ(50, wagon1.max);
}

TEST(Train_f, add_wagon){
	Train train, t;
	Wagon wagon, w;
	wagon.max = 10;
	wagon.number = 5;
	t.Wagon_to_train(wagon);
	train += t;
	w = train.get_wagon(1);
	EXPECT_EQ(5, w.number);
	EXPECT_EQ(10, w.max);
	wagon.max = 2;
	ASSERT_ANY_THROW(t.Wagon_to_train(wagon););
	wagon.number = -1;
	ASSERT_ANY_THROW(t.Wagon_to_train(wagon););
}

TEST(Train_f, take_seat){
	Train train, t;
	Wagon wagon, w;
	wagon.max = 10;
	t.Wagon_to_train(wagon);
	train += t;
	train(1, 5);
	w = train.get_wagon(1);
	EXPECT_EQ(5, w.number);
	ASSERT_ANY_THROW(train(2, 3));
	ASSERT_ANY_THROW(train(1, 100));
	ASSERT_ANY_THROW(train(1, -1));
}

TEST(Train_f, passengers_in_wagon){
	Train train, t;
	Wagon wagon, w;
	wagon.max = 10;
	t.Wagon_to_train(wagon);
	train += t;
	w = train.get_wagon(1);
	EXPECT_EQ(0, train[1]);
	ASSERT_ANY_THROW(train[0]);
}

TEST(Train_f, Replace_wagons){
	Train train, train1, t;
	Wagon wagon;
	wagon.max = 10;
	wagon.number = 5;
	t.Wagon_to_train(wagon);
	train += t;
	train += t;
	train += t;
	int mass[2] = { 1, 3 };
	train(2, mass, train1);
	EXPECT_EQ(1, train.get_number());
	EXPECT_EQ(2, train1.get_number());
	ASSERT_ANY_THROW(train1(2, mass, train1););
}

TEST(Train_f, print){
	ostringstream str1;
	Train train;
	char * st = "8\n7\n7\n6\n6\n5\n5\n4\n4\n";
	istringstream str(st);
	str >> train;
	str1 << train;
	char *str2 = "Number of passengers in wagons: \nThe 1th wagon: 7\nThe 2th wagon: 6\nThe 3th wagon: 5\nThe 4th wagon: 4";
	string str3 = str1.str();
	for (int i = 0; i < 100; i++)
		EXPECT_EQ(str2[i], str3[i]);
}

TEST(Train_f, enterinig){
	Train train;
	int i, j, code;
	Wagon w;
	char * st = "8\n7\n7\n6\n6\n5\n5\n4\n4\n3\n3\n2\n2\n1\n";
	istringstream str(st);
	str >> train;
	for (i = 0, j = 1; i < 14; j++){
		code = st[i * 2];
		w = train.get_wagon(j);
		EXPECT_EQ(code - 48, w.max);
		i++;
		code = st[i * 2];
		EXPECT_EQ(code - 48, w.number);
		i++;
	}
	Train train1;
	char * st1 = "8\n7\n7\n8\n";
	istringstream str1(st1);
	str1 >> train1;
	w = train1.get_wagon(1);
	EXPECT_EQ(8, w.max);
	EXPECT_EQ(7, w.number);
	EXPECT_EQ(1, train1.get_number());
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

