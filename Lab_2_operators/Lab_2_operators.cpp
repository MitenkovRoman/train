
#include "stdafx.h"
#include "..\Library\Train.h"
#include <iostream>

using namespace std;

const char *msgs[] = { "0. Quit", "1. Initialization of train", "2. Number of passengers in wagons", "3. Take seats in wagon", 
"4. Number of passengers in wagon", "5. Add wagon", "6. Replace some wagons and make new train", "7. Number of all passengers in train" };
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

int getNum(int &a){// 0-�������, 1-����� �����, -1-������������ ����
	cin >> a;
	if (!cin.good()){
		if (cin.eof()){
			cout << "End of file!!!" << endl;
			return 1;
		}
		cout << "Incorrect data!!!" << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		return -1;
	}
	return 0;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;

	do{
		puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";

		// ����� ������ �����������
		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		cout << "Make your choice: --> ";

		n = getNum(rc); // ���� ������ ������������
		if (n == 1) // ����� ����� - ����� ������
			return 0;
	} while (n == -1 || rc < 0 || rc >= N);

	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail()); // �������, ��� �������������, ��� ���������� ������� �� ������ �����

	return rc;
}

int Initialization(Train & train){
	cin >> train;
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	return 1;
}

int Passengers_in_wagons(Train & train){
	cout << train;
	return 1;
}

int Take_seats(Train & train){
	int number, fl, places;
	do{
		cout << "Enter number of wagon: ";
		fl = getNum(number);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	do{
		cout << "Enter amount of places: ";
		fl = getNum(places);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	try{
		train(number, places);
	}
	catch (const char * ms){
		cout << ms << endl;
	}
	return 1;
}

int Passengers_in_wagon(Train & train){
	int number, fl, res;
	do{
		cout << "Enter number of wagon: ";
		fl = getNum(number);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	try{
		res = train[number];
	}
	catch (const char * ms){
		cout << ms << endl;
		return 1;
	}
	cout << "Number of passengers: " << res << endl;
	return 1;
}

int Add_wagon(Train & train){
	Train t;
	int max, fl, number;
	do{
		cout << "Enter max amount of passengers in wagon: ";
		fl = getNum(max);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	do{
		cout << "Enter number of passengers: ";
		fl = getNum(number);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	Wagon wagon(max, number);
	try{
		t.Wagon_to_train(wagon);
		train += t;
	}
	catch (const char * ms){
		cout << ms << endl;
	}
	return 1;
}

int Replace_wagons(Train & train){
	int fl, n, *mass;
	char * ms;
	do{
		cout << "Enter number of wagons you want to replace: ";
		fl = getNum(n);
		if (fl == 1)
			return 1;
	} while (fl != 0);
	try{
		mass = new int[n];
	}
	catch (bad_alloc &ba){
		cout << ba.what() << endl;
		return 1;
	}
	for (int i = 0; i < n; i++){
		ms = "";
		do{
			cout << ms << endl;
			ms = "Incorrect data!!!";
			cout << "Enter number of wagon:";
			fl = getNum(mass[i]);
			if (fl == 1)
				return 1;
		} while (fl != 0);
	}
	Train new_train;
	try{
		train(n, mass, new_train);
	}
	catch (const char * ms){
		cout << ms << endl;
		delete[] mass;
		return 1;
	}
	cout << "Train:\n" << train;
	cout << "New Train: \n" << new_train;
	delete[] mass;
	return 1;
}

int All_passengers(Train & train){
	cout << "Number of all passengers: " << train.Passengers() << endl;
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Train inv;
	int rc;
	int(*menu[])(Train &) = { NULL, Initialization, Passengers_in_wagons, Take_seats, Passengers_in_wagon, Add_wagon, Replace_wagons, All_passengers};
	while (rc = dialog(msgs, NMsgs))
		if (!menu[rc](inv))
			break;
	return 0;
}

